<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookAddRequest;

class BookController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function add(BookAddRequest $request)
    {
        $data = $request->input();

        dd($data);
    }
}
